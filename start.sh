#!/bin/bash

containerName="demoapp"
# Webserver folder
workDir="/usr/share/nginx/html/"

# Local port -> webserver port
port="1234:80"

# Lokal directory -> webserver dir
volume="$(pwd)/dist:$workDir"
echo $volume

docker build --tag $containerName .

docker run --name $containerName --workdir $workDir -p $port --volume $volume --rm --detach $containerName
