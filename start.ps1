    $containerName = "demoapp"
    # Webserver folder
    $workDir = "/usr/share/ngnix/html/"

    # Local port -> webserver port
    $port = "1234:80"

    # Lokal directory -> webserver dir
    $volume = "$PWD/dist:$workDir"

    docker build --tag $containerName .

    docker run --name $containerName --workdir $workDir -p $port --volume $volume --rm --detach $containerName
